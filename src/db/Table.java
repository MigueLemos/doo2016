/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author lab6
 */
public class Table extends HashMap implements TableModel{


    protected Integer columnas;
    
    Table() {}
    private static void procRSMD(Table bb, ResultSetMetaData rsmd) throws SQLException {
        bb.columnas = rsmd.getColumnCount();
        ArrayList<String> colNameList;
        ArrayList<String> colTypeList;
        ArrayList<String> colClassList;
        ArrayList<Integer> colLongList;
        bb.put("colNameList", (colNameList = new ArrayList<>()));
        bb.put("colTypeList", (colTypeList = new ArrayList<>()));
        bb.put("colClassList", (colClassList = new ArrayList<>()));
        bb.put("colLongList", (colLongList = new ArrayList<>()));
        for(Integer i = 1; i<=bb.columnas; i++){
            bb.put(i, rsmd.getColumnName(i));
            bb.put(rsmd.getColumnName(i), i);
            colNameList.add(rsmd.getColumnName(i));
            colTypeList.add(rsmd.getColumnTypeName(i));
            colClassList.add(rsmd.getColumnClassName(i));
            colLongList.add(rsmd.getColumnDisplaySize(i));
        }
        
    }
    
    protected static void procRS(Table bb, ResultSet rs) throws SQLException {
        ArrayList<ArrayList<Comparable>> datosTabla = new ArrayList<>();
        ArrayList<Comparable> fila;
        while(rs.next()){
            fila = new ArrayList<>();
            for(Integer i = 1; i<=bb.columnas; i++){
                fila.add((Comparable)rs.getObject(i));
            }
            datosTabla.add(fila);
        }
        bb.put("datosTabla", datosTabla);
    }
    
    public static Table select(String sql) throws ClassNotFoundException, ClassNotFoundException, Exception{
        Table bb=new Table();
        ResultSet rs = DBChannel.getCanalDB("org.apache.derby.jdbc.EmbeddedDriver",
                    "jdbc:derby:derbyDB;create=false", "", "").executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        procRSMD(bb, rsmd);
        procRS(bb, rs);
        return bb;
    }
    public static Table execute(Object obj){
        Table bb=new Table();
        return bb;
    }

    @Override
    public int getRowCount() {
        return ((List)get("datosTabla")).size();
    }

    @Override
    public int getColumnCount() {
        return columnas;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return ((List)get("colNameList")).get(columnIndex).toString();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        try {
            return Class.forName(((List)get("colClassList")).get(columnIndex).toString());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return ((List)((List)get("datosTabla")).get(rowIndex)).get(columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ((List)((List)get("datosTabla")).get(rowIndex)).set(columnIndex, aValue);
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
