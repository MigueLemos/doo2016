/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doo2016;

import db.DBChannel;
import db.Table;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import widget.WFac;
import widget.Widget;

/*
 *
 * @author mc
 */
public class Doo2016 implements WindowListener, Runnable {

    private final JFrame jf;
    private final Widget wid;

    public Doo2016() {
        Dimension tk = Toolkit.getDefaultToolkit().getScreenSize();
        jf = new JFrame("Prueba");
        jf.setLayout(null);
        jf.addWindowListener(this);
        jf.add(wid = WFac.createWPanel("<html><h3>El gran panel</h3></html>", "r1"));
        wid.addW(WFac.createWTexto1l("El rótulo1", "n1"));
        wid.addW(WFac.createWTextoNl("El rótulo 2", "n2"));
        wid.addW(WFac.createWBoton("El botón", "bot1", this, "MetBoton"));
        Widget widPB;
        wid.addW(widPB = WFac.createWPanelB("PBU", "pbot1"));
        widPB.addW(WFac.createWBoton("Boton", "bot2", this, "MetBoton"));
        widPB.addW(WFac.createWBoton("Boton2", "bot3", this, "MetBoton"));
        Widget widTabla;
        wid.addW(widTabla =WFac.createWTable("Tabla de cosas", "tab1"));
        jf.setExtendedState(JFrame.MAXIMIZED_BOTH);
        jf.setLocation((int) ((tk.width - jf.getWidth()) / 2), (int) ((tk.height - jf.getHeight()) / 2));
        Map<String, Object> mapa = new TreeMap<>();
        Object M = null;
        try {
//            stm.execute("drop table a");
//            stm.execute("create table a(id int primary key, pname varchar(100), color varchar(12), ayuda varchar(2000), borrado boolean)");
//            for(int i = 1; i<101; i++){
//                stm.execute("insert into a values("+i+",'r1','0x1f1f1f','Esto es un panel',false)");
//            }
            int start = 0;
            int rows = 12;
            widTabla.set(Table.select("select * from a order by ID OFFSET "+start+" ROWS FETCH NEXT "+rows+" ROWS ONLY"));
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Doo2016.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Doo2016.class.getName()).log(Level.SEVERE, null, ex);
        }
        wid.set(mapa);
        jf.setVisible(true);   
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        SwingUtilities.invokeLater(new Doo2016());
    }

    @Override
    public void windowOpened(WindowEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.exit(0); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowIconified(WindowEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void MetBoton() {
        System.out.println(wid.get());
    }
}