package widget;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
public class WTextoNl extends Widget {

    public WTextoNl(String rotu) {
        super(rotu);
        JScrollPane comp1 = new JScrollPane(comp=new JTextArea());
        comp1.setBounds(WFac.anchoRo+2*WFac.esp, WFac.esp, WFac.ancho-WFac.anchoRo-5*WFac.esp, WFac.altoTA);
        setSize(WFac.ancho, WFac.altoTA+2*WFac.esp);
        add(comp1);
    }

    @Override
    public void set(Object p0) {
        ((JTextArea)comp).setText(p0.toString());
    }

    @Override
    public Object get() {
        return ((JTextArea)comp).getText();
    }

}
