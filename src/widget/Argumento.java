package widget;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lab6
 */
public class Argumento {
    public final String metodo;
    public final Object objeto;
    private Class[] cla=(Class[])null;
    private Object[] obj = (Object[]) null;
    public Argumento(String metodo, Object objeto){
        this.metodo = metodo;
        this.objeto = objeto;
    }

    /**
     * @return the cla
     */
    public Class[] getCla() {
        return cla;
    }

    /**
     * @param cla the cla to set
     */
    public void setCla(Class[] cla) {
        this.cla = cla;
    }

    /**
     * @return the obj
     */
    public Object[] getObj() {
        return obj;
    }

    /**
     * @param obj the obj to set
     */
    public void setObj(Object[] obj) {
        this.obj = obj;
    }
    
    public Object exec(String mens){
        try {
                return objeto.getClass().getMethod(metodo, cla).invoke(objeto, obj);
            } catch (Exception ex) {
                Logger.getLogger(WBoton.class.getName()).log(Level.SEVERE, "Error en "+ mens + " invocando " + objeto.getClass().getSimpleName() +"."+metodo, ex);
       }    
        return null;
    }
    
    public static Map<String, Argumento> mapaAcc(Argumento ab, String nombre, Map<String,Argumento> mapa){
        try{
            mapa.equals(mapa);
        }catch(Exception ex){
            mapa = new HashMap<>();
        }
        mapa.put(nombre, ab);
        return mapa;
    }
}
