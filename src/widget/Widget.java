package widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;

public abstract class Widget extends JPanel implements KeyListener, MouseListener, MouseMotionListener{

    protected JLabel rotulo;
    protected Component comp;
    protected Map<String, Argumento> mapaEv;

    protected Widget(String rotu) {
        setBackground(Color.LIGHT_GRAY);
        rotulo= new JLabel(rotu);
        setLayout(null);
        setBounds(0, 0, WFac.ancho, WFac.alto);
        rotulo.setBounds(WFac.esp, WFac.esp, WFac.anchoRo, WFac.altoRo);
        add(rotulo);
    }

    public void addW(Widget w){ }

	/**
	 *  
	 */
    public abstract void set(Object p0);

    /**
     *  
     */
    public abstract Object get();

    /**
     * @return the rotulo
     */
    public JLabel getRotulo() {
        return rotulo;
    }

    /**
     * @param rotulo the rotulo to set
     */
    public void setRotulo(JLabel rotulo) {
        this.rotulo = rotulo;
    }

    /**
     * @return the comp
     */
    public Component getComp() {
        return comp;
    }

    /**
     * @param comp the comp to set
     */
    public void setComp(Component comp) {
        this.comp = comp;
    }
    
    public void addEvMap(Object ob, String met, String ev){
        mapaEv=Argumento.mapaAcc(new Argumento(met, ob), ev, mapaEv);
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
       
    }

    @Override
    public void keyPressed(KeyEvent e) {
        try {
            mapaEv.get("key_"+Integer.toString(e.getKeyCode())).exec(getName());
        } catch (Exception ex) {
            
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       
    }

    @Override
    public void mousePressed(MouseEvent e) {
        try {
            mapaEv.get("mc_"+Integer.toString(e.getButton())).exec(getName());
        } catch (Exception ex) {
            
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       
      }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        
    }

}
