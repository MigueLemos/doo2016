/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package widget;

import java.awt.Toolkit;

/**
 *
 * @author mc
 */
public class WFac {
    private static Toolkit tk=Toolkit.getDefaultToolkit();
    static Integer ancho=(int)(tk.getScreenSize().width),
            esp=4,
            alto=26, 
            altoTA=alto*5-2*esp+1,
            anchoRo=(int)(ancho*.3),
            altoRo=alto-2*esp
            ;
    private WFac(){}
    
    public static Widget createWPanel(String rotulo,String name){
        Widget wp = new WPanel(rotulo);
        wp.setName(name);
        
        return wp;
                
    }
    public static Widget createWTable(String rotulo,String name){
        Widget wp = new WTable(rotulo);
        wp.setName(name);
        return wp;
    }
    
    public static Widget createWPanelB(String rotulo,String name){
        Widget wp = new WPanelB(rotulo);
        wp.setName(name);
        return wp;
                
    }
    public static Widget createWBoton(String rotulo, String name, Object objeto, String metodo){
        Widget wp = new WBoton(rotulo);
        wp.setName(name);
        Argumento ab = new Argumento(metodo, objeto);
        wp.set(ab);
        return wp;
    }    
    public static Widget createWTexto1l(String rotulo,String name){
        Widget wp = new WTexto1l(rotulo);
        wp.setName(name);
        
        return wp;
                
    }
    
    public static Widget createWTextoNl(String rotulo,String name){
        Widget wp = new WTextoNl(rotulo);
        wp.setName(name);
        
        return wp;
                
    }
}
