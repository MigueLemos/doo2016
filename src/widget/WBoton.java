/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package widget;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
/**
 *
 * @author mc
 */
public class WBoton extends Widget implements ActionListener {
    private Argumento ab;
    WBoton(String rotu) {
        
        super("");
        comp= new JButton(rotu);
        this.rotulo.setVisible(false);
        setBounds(0,0,WFac.anchoRo,WFac.alto);
        comp.setBounds(WFac.esp,WFac.esp,WFac.anchoRo-WFac.esp*4,WFac.alto-2*WFac.esp);
        add(comp);
        ((JButton)comp).addActionListener(this);
        ((JButton)comp).setBackground(Color.gray);
        
    }

    @Override
    public void set(Object p0) {
        ab = (Argumento) p0;
    }

    @Override
    public Object get() {
        return ab;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ab.exec(getName());
    }
    
}
