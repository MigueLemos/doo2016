package widget;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WPanelB extends Widget {

    protected List<Widget> wlist;
    protected Map<String, Widget>wmap;

    @Override
    public void set(Object p0) {
        Map<String, Object> mapa;
        try{
            mapa=(Map<String, Object>) p0;
            for(String s : mapa.keySet())
                wmap.get(s).set(mapa.get(s));
        }catch(Exception ex){
            
        }
        
    }

    @Override
    public Object get() {
        Map<String, Object> mapa= new TreeMap<>();
        for (Widget s : wlist)
            mapa.put(s.getName(), s.get());
        
        return mapa;
    }

    WPanelB(String rotulo) {
        super(rotulo);
        getRotulo().setVisible(false);
        setSize(3*WFac.esp,WFac.alto);
        wlist = new ArrayList<>();
        wmap  = new HashMap<>();
    }
    
    @Override
    public void addW(Widget w){
        Dimension d = getSize();
        wlist.add(w);
        wmap.put(w.getName(), w);
        w.setLocation(d.width-WFac.esp, 0);
        setSize(getWidth()+w.getWidth()-3*WFac.esp,getHeight());
        add(w);
    }

}
