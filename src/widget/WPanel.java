package widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JLabel;

public class WPanel extends Widget {

    protected List<Widget> wlist;
    protected Map<String, Widget>wmap;

    @Override
    public void set(Object p0) {
        Map<String, Object> mapa;
        try{
            mapa=(Map<String, Object>) p0;
            for(String s : mapa.keySet())
                wmap.get(s).set(mapa.get(s));
        }catch(Exception ex){
            
        }
        
    }

    @Override
    public Object get() {
        Map<String, Object> mapa= new TreeMap<>();
        for (Widget s : wlist)
            mapa.put(s.getName(), s.get());
        
        return mapa;
    }

    WPanel(String rotulo) {
        super(rotulo);
        JLabel rt;
        setBackground(Color.GRAY);
        (rt=getRotulo()).setSize(WFac.ancho-2*WFac.esp, WFac.altoRo);
        rt.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        wlist = new ArrayList<>();
        wmap  = new HashMap<>();
    }
    
    @Override
    public void addW(Widget w){
        Point p = new Point(0,0);
        Dimension d = getSize();
//        System.out.println(p.toString()+" - "+d.toString());
        wlist.add(w);
        wmap.put(w.getName(), w);
        w.setLocation(WFac.esp, p.y+d.height+WFac.esp);
        w.setSize(w.getWidth()-WFac.esp*2, w.getHeight());
        this.setSize(this.getWidth(),this.getHeight()+w.getHeight()+WFac.esp*2);
        add(w);
    }

}
