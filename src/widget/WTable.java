/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package widget;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import db.Table;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 *
 * @author lab6
 */
public class WTable extends Widget{

    public WTable(String rotu) {
        super(rotu);
        JScrollPane comp1 = new JScrollPane(comp= new JTable());
        
        setBounds(0, 0, WFac.ancho, WFac.altoTA*2);
        comp1.setBounds(WFac.esp, WFac.altoRo + 2*WFac.esp, WFac.ancho-4*WFac.esp, WFac.altoTA*2 - WFac.altoRo - 3*WFac.esp);
        comp.setBounds(WFac.esp, WFac.altoRo + 2*WFac.esp, WFac.ancho-4*WFac.esp, WFac.altoTA*2 - WFac.altoRo - 3*WFac.esp);
        add(comp1);
        rotulo.setHorizontalTextPosition(SwingConstants.CENTER);
        rotulo.setLocation(WFac.ancho/2, WFac.esp);
    }
    
    @Override
    public void set(Object p0) {
       ((JTable)comp).setModel((TableModel)p0);
    }

    @Override
    public Object get() {
        return null;
     }        
}
