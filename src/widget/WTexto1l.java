package widget;

import java.awt.Color;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.io.Console;
import java.util.HashMap;
import javax.swing.FocusManager;
import javax.swing.JColorChooser;
import javax.swing.JTextField;

public class WTexto1l extends Widget {
    WTexto1l(String rotu) {
        super(rotu);
        comp= new JTextField();
        comp.setBounds(WFac.anchoRo+2*WFac.esp, WFac.esp, WFac.ancho-WFac.anchoRo-5*WFac.esp, WFac.altoRo);
        add(comp);
        comp.addKeyListener(this);
        addEvMap(this, "execEv_key_10_40", "key_10");
        addEvMap(this, "execEv_key_10_40", "key_40");
        addEvMap(this, "execEv_key_38", "key_38");
        
        addMouseListener(this);
        addEvMap(this, "execEv_mc_1", "mc_1");
        addEvMap(this, "execEv_mc_2", "mc_2");
        addEvMap(this, "execEv_mc_3", "mc_3");
    }

    @Override
    public void set(Object p0) {
        ((JTextField)comp).setText(p0.toString());
    }

    @Override
    public Object get() {
        return ((JTextField)comp).getText();
    }
    

    public void execEv_key_10_40(){
        FocusManager.getCurrentManager().focusNextComponent();
    }
    public void execEv_key_38(){
        FocusManager.getCurrentManager().focusPreviousComponent();
    }    
    public void execEv_mc_1 () {
        FocusManager.getCurrentManager().focusNextComponent();
    }
    public void execEv_mc_2(){
        setBackground(JColorChooser.showDialog(comp, "Color de fondo", getBackground()));
    }
    public void execEv_mc_3 () {
        FocusManager.getCurrentManager().focusPreviousComponent();
    }
}
